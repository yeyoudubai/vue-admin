# vue-element-admin-change  #

## 项目简介

## 在vue-element-admin集成版本上进行了相应的简化依赖，剔除无用组件。 ##

## 新增@/settings.js文件中的routesRole来切换前端或者后端控制路由 （默认前端控制） ##

1. 新增v-dialog指令，使对话框能够拖拽
2. 全局按钮级权限指令v-permission
3. 增加tagView左右滑动，快捷关闭操作
4. 更新配置@/settings.js文件中的routesRole来切换前端或者后端控制路由-动态路由 （已改为默认前端控制）
5. 更新布局
## 加入我们 ##

[https://jq.qq.com/?_wv=1027&k=MfdeOae1](https://jq.qq.com/?_wv=1027&k=MfdeOae1)

### 项目截图

![](https://s1.ax1x.com/2020/06/11/t7xdHA.png)

![](https://s1.ax1x.com/2020/06/11/t7xYcD.png)

![](https://s1.ax1x.com/2020/06/11/t7xUnH.png)

![](https://s1.ax1x.com/2020/06/11/t7xJ1O.png)

![](https://s1.ax1x.com/2020/06/11/t7xtje.png)

![](https://s1.ax1x.com/2020/06/11/t7xaBd.png)

## 演示地址 ##

[https://yeyoudubai.gitee.io/vue-wvadmin](https://yeyoudubai.gitee.io/vue-admin)

### 运行

1. npm install

2. npm run dev

