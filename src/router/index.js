import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)
/*
  //当设置 true 的时候该路由不会在侧边栏出现 如401，login等页面，或者如一些编辑页面/edit/1
  hidden: true // (默认 false)

  //当设置 noRedirect 的时候该路由在面包屑导航中不可被点击
  redirect: 'noRedirect'

  //当你一个路由下面的 children 声明的路由大于1个时，自动会变成嵌套的模式--如组件页面
  //只有一个时，会将那个子路由当做根路由显示在侧边栏--如引导页面
  //若你想不管路由下面的 children 声明的个数都显示你的根路由
  //你可以设置 alwaysShow: true，这样它就会忽略之前定义的规则，一直显示根路由
  alwaysShow: true

  name: 'router-name' //设定路由的名字，一定要填写不然使用<keep-alive>时会出现各种问题
  meta: {
    roles: ['admin', 'editor'] //设置该路由进入的权限，支持多个权限叠加
    title: 'title' //设置该路由在侧边栏和面包屑中展示的名字
    icon: 'svg-name' //设置该路由的图标
    noCache: true //如果设置为true，则不会被 <keep-alive> 缓存(默认 false)
    breadcrumb: false // 如果设置为false，则不会在breadcrumb面包屑中显示
  }

 */
/* Layout */
import Layout from '@/layout'

/* 公共路由 */
export const constantRoutes = [
  // 刷新页面
  {
    path: '/redirect',
    component: Layout,
    hidden: true,
    children: [{
      path: '/redirect/:path(.*)',
      component: () => import('@/views/redirect/index')
    }]
  },
  // 首页 控制台
  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [{
      path: 'dashboard',
      component: () => import('@/views/dashboard/index'),
      name: 'Dashboard',
      hidden: true,
      meta: {
        title: '控制台',
        icon: 'dashboard',
        affix: true
      }
    }]
  },
  // 个人中心
  {
    path: '/profile',
    component: Layout,
    redirect: '/profile/index',
    hidden: true,
    children: [
      {
        path: 'index',
        component: () => import('@/views/profile/index'),
        name: 'Profile',
        meta: { title: 'Profile', icon: 'user', noCache: true }
      }
    ]
  },
  // 登录
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },
  // 更改权限刷新页面
  {
    path: '/auth-redirect',
    component: () => import('@/views/login/auth-redirect'),
    hidden: true
  },
  {
    path: '/404',
    component: () => import('@/views/error-page/404'),
    hidden: true
  },
  {
    path: '/401',
    component: () => import('@/views/error-page/404'),
    hidden: true
  }
]

/* 权限路由 */
export const asyncRoutes = [{
  path: '/banner',
  component: Layout,
  name: 'Banner',
  meta: {
    title: '广告',
    icon: 'dashboard'
  },
  children: [{
    path: 'index',
    component: () => import('@/views/banner/index'),
    name: 'BannerIndex',
    meta: {
      title: '轮播图',
      icon: 'dashboard'

    }
  }, {
    path: 'fictitious',
    component: () => import('@/views/banner/fictitious'),
    name: 'Fictitious',
    meta: {
      title: '虚拟数据',
      icon: 'dashboard'

    }
  }]
},
{
  path: '/product',
  component: Layout,
  name: 'Product',
  meta: {
    title: '产品',
    icon: 'dashboard'
  },
  children: [{
    path: 'index',
    component: () => import('@/views/product/index'),
    name: 'ProductIndex',
    meta: {
      title: '普通产品',
      icon: 'dashboard'

    }
  }, {
    path: 'vip',
    component: () => import('@/views/product/vip'),
    name: 'VipProduct',
    meta: {
      title: 'VIP产品',
      icon: 'dashboard'
    }
  }]
},
{
  path: '/animate',
  component: Layout,
  name: 'Animate',
  meta: {
    title: '动画',
    icon: 'dashboard'
  },
  children: [{
    path: 'index',
    component: () => import('@/views/animate/index'),
    name: 'AnimateIndex',
    meta: {
      title: '过渡动画',
      icon: 'dashboard'
    }
  },
  {
    path: 'loading',
    component: () => import('@/views/animate/loading'),
    name: 'LoadingIndex',
    meta: {
      title: '加载动画',
      icon: 'dashboard'
    }
  }]
},
{
  path: '/power',
  component: Layout,
  name: 'Power',
  meta: {
    title: '权限',
    icon: 'dashboard'
  },
  children: [{
    path: 'index',
    component: () => import('@/views/power/index'),
    name: 'PowerIndex',
    meta: {
      title: '权限设置',
      icon: 'dashboard'

    }
  }, {
    path: 'role',
    component: () => import('@/views/power/role'),
    name: 'Role',
    meta: {
      title: '角色管理',
      icon: 'dashboard'

    }
  }]
}, {
  name: '404页面',
  path: '*',
  redirect: '/dashboard',
  hidden: true
}
]

const createRouter = () =>
  new Router({
    // mode: 'history', // require service support
    // scrollBehavior含义
    // https://www.jianshu.com/p/c805b74e1f14
    // https://router.vuejs.org/zh/guide/advanced/scroll-behavior.html
    // 回到顶部
    scrollBehavior: () => ({
      y: 0
    }),
    routes: constantRoutes
  })

const router = createRouter()

// 重设路由 https://blog.csdn.net/suolong914/article/details/89432563
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher
}

export default router
